---
title: About
date: '2023-10-08'
author: Sarah Paul 
---
[UnbOS](./_index.md) is a progressive initiative that aims to facilitate access to essential software packages and dependencies for students at the National Institute of Technology Calicut (NITC). UnbOS is designed to provide students, regardless of their academic discipline, with comprehensive access to a curated selection of software tools that align with the academic requirements at NITC.
  
UnbOS utilizes the [Calamares](https://calamares.io/) installer for a seamless and user-friendly installation process. Furthermore, it features the GNOME desktop environment to offer an intuitive and robust graphical user interface.
  
Our goal is to empower NITC students with the essential software applications required to excel in their academic pursuits. These carefully curated software resources will be made readily accessible through UnbOS, ensuring that students from various disciplines have the necessary tools to succeed in their studies. Some of the tools are listed below sorted in order of branch.
  
 ## CSE
- [VS code](https://code.visualstudio.com/) 
- [Docker](https://www.docker.com/)
- [NASM](https://en.wikipedia.org/wiki/Netwide_Assembler) 
- [MATLAB](https://en.wikipedia.org/wiki/Netwide_Assemble)
- [ModelSim ALtera](https://www.intel.com/content/www/us/en/software-kit/750368/modelsim-intel-fpgas-standard-edition-software-version-18-1.html)  
---
## ECE
Quartus Prime\
Vivado \
LTspice\
Silvaco\
Cadence\
Proteus\
KiCad\

## EEE
MPLAB
