---
title: unbOS
toc: true 
---
![](/Pages/Home/unbOSlogo.png)
At UnbOS, our mission is to create a Linux distribution that's perfectly attuned to the needs of students. We want to make sure you have access to all the software packages and tools you'll require throughout your entire four-year academic journey.

If you'd like to stay updated on our progress and learn more about what we're working on, please visit our [Github Page](https://github.com/gnu-unbOS/). It's where you can discover the latest developments and get a behind-the-scenes look at what makes unbOS a great resource for students.




